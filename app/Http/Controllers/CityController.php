<?php

namespace App\Http\Controllers;

use App\Models\Common\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $cities = new City;

        if ($request->filled('state')) {
            $cities = $cities->where('state_id', $request->state);
        }

        $cities = $cities->get();

        return response($cities, 200);
    }
}
