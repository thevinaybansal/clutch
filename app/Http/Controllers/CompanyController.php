<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCompanyRequest;
use App\Models\Auth\User;
use App\Models\Company\Company;
use App\Models\Company\Portfolio;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    public function getServices(Company $company)
    {
        return response($company->services, 200);
    }

    public function getIndustries(Company $company)
    {
        return response($company->industries, 200);
    }

    public function getLocations(Company $company)
    {
        return response($company->locations, 200);
    }

    public function find($id)
    {
        $company = Company::find($id);
        return response($company, 200);
    }

    public function edit(Company $company)
    {
        return view('company.edit')->with('company', $company);
    }

    public function index()
    {
        $companies = Company::latest()->get();
        return view('company.index')->with('companies', $companies);
    }

    public function getPortfolios(Company $company)
    {
        return response($company->portfolios, 200);
    }

    public function save(CreateCompanyRequest $request)
    {
        switch ($request->step) {
            case 'step-1':
                $inputs = [
                    'name' => $request->company_name,
                    'min_project_size' => $request->min_project_size,
                    'hourly_rate' => $request->hourly_rate,
                    'company_size' => $request->company_size,
                    'year_founded' => $request->year_founded,
                    'tagline' => $request->tagline,
                    'summary' => $request->summary,
                    'slug' => Str::slug($request->company_name),
                ];
                // check if company already created
                if ($request->id) {
                    $company = Company::find($request->id);
                    $company->update($inputs);
                } else {
                    $company = Company::create($inputs);
                }
                return response($company, 200);

            case 'step-2':
                $company = Company::find($request->id);
                foreach ($request->locations as $location) {
                    $company->locations()->create([
                        'country_id' => $location['country'],
                        'address' => $location['address'],
                        'city' => $location['city'],
                        'state' => $location['state'],
                        'address' => $location['address'],
                        'postal_code' => $location['postal_code'],
                        'phone_number' => $location['phone'],
                        'is_headquarter' => $location['is_head'],
                        'slug' => Str::slug($location['address']),
                    ]);
                }
                return response($company->locations, 200);

            case 'step-3':
                $company = Company::find($request->id);
                $company->update([
                    'website' => $request->website,
                    'email' => $request->email,
                ]);
                return response($company, 200);

            case 'step-4':
                $company = Company::find($request->id);
                // save services
                foreach($request->services as $service) {
                    $company->services()->attach($service['id'], [
                        'focus_percentage' => $service['percentage'],
                    ]);
                }
                // save industries
                foreach($request->industries as $industry) {
                    $company->industries()->attach($industry['id'], [
                        'focus_percentage' => $industry['percentage'],
                    ]);
                }

                return response([
                    'services' => $company->services,
                    'industries' => $company->industries,
                ], 200);

            case 'step-5':
                $company = Company::find($request->id);
                $portfolio = $company->portfolios()->create([
                    'slug' => Str::slug($request->title),
                    'user_id' => User::find(1)->id,
                    'image' => $request->file('image')->store('portfolio', 'public'),
                    'title' => $request->title,
                    'summary' => $request->description,
                    'link' => $request->title,
                ]);

                return response($portfolio, 200);

            case 'step-6':
                $company = Company::find($request->id);
                $company->update([
                    'email' => $request->admin_email,
                    'phone' => $request->admin_phone,
                    'linkedin' => $request->linkedin_url,
                    'twitter' => $request->twitter_url,
                    'facebook' => $request->facebook_url,
                    'instagram' => $request->instagram_url,
                ]);

            default:
                break;
        }
    }

    public function create()
    {
        return view('company.create');
    }
}
