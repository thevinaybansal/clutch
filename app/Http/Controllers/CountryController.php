<?php

namespace App\Http\Controllers;

use App\Models\Common\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        return response(Country::orderBy('name', 'asc')->get(), 200);
    }
}
