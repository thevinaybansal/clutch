<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company\Industry;
use App\Http\Requests\CreateIndustryRequest;
use App\Http\Requests\UpdateIndustryRequest;

class IndustryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $industries = Industry::all();
            return response($industries, 200);
        } else {
            $industries = Industry::paginate(10);
            return view('industries.index')->with(compact('industries'));
        }
    }

    public function create()
    {
        return view('industries.create');
    }


    public function store(CreateIndustryRequest $request)
    {
        Industry::create($request->all());
        return redirect('industries')->with('success', 'Industry Successfully Created!');
    }


    public function edit($id)
    {
        $industry = Industry::find($id);
        return view('industries.edit')->with(compact('industry'));
    }


    public function update(UpdateIndustryRequest $request, $id)
    {
        $industry = Industry::find($id);

        if($industry) {
            $industry->update($request->all());
        }

        return redirect('industries')->with('success', 'Industry Successfully Updated!');
    }

    public function destroy($id)
    {
        $industry = Industry::find($id);

        if($industry) {
            $industry->delete();
        }

        return redirect('industries')->with('success', 'Industry Successfully Deleted!');
    }


}
