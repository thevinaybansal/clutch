<?php

namespace App\Http\Controllers;

use App\Models\Company\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function delete(Portfolio $portfolio)
    {
        $portfolio->delete();
        return response(['message' => 'Portfolio item deleted.'], 200);
    }
}
