<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company\Service;
use App\Http\Requests\CreateServiceRequest;
use App\Http\Requests\UpdateServiceRequest;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $services = Service::all();
            return response($services, 200);
        } else {
            $services = Service::paginate(10);
            return view('services.index')->with(compact('services'));
        }
    }

    public function create()
    {
        return view('services.create');
    }


    public function store(CreateServiceRequest $request)
    {
        $request->validated();

        Service::create($request->all());
        return redirect('services')->with('success', 'Service Successfully Created!');
    }


    public function edit($id)
    {
        $service = Service::find($id);
        return view('services.edit')->with(compact('service'));   
    }


    public function update(UpdateServiceRequest $request, $id)
    {
        $request->validated();

        $service = Service::find($id);

        if($service)
        {
            $service->update($request->all());
        }

        return redirect('services')->with('success', 'Service Successfully Updated!');
    }

    public function destroy($id)
    {
        $service = Service::find($id);

        if($service)
        {
            $service->delete();
        }

        return redirect('services')->with('success', 'Service Successfully Deleted!');
    }


}
