<?php

namespace App\Http\Controllers;

use App\Models\Common\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function index(Request $request)
    {
        $states = new State;

        if ($request->filled('country')) {
            $states = $states->where('country_id', $request->country);
        }

        $states = $states->get();

        return response($states, 200);
    }
}
