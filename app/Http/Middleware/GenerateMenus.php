<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('AdminLeftSideMenu', function ($menu) {
            
            $menu->add('Dashboard' , ['icon'=>'flaticon-layers']);
            $menu->get('dashboard')->add('Dashboard' , ['url' => '/', 'icon'=>'flaticon-layers']);

            $menu->add('Company' , ['icon'=>'flaticon-layers']);
            $menu->get('company')->add('Companies', ['url' => 'companies', 'icon' => 'flaticon-layers']);
            $menu->get('company')->add('New Company', ['url' => 'company/create', 'icon' => 'flaticon-layers']);

            $menu->add('Industries' , ['icon'=>'flaticon-layers']);
            $menu->get('industries')->add('Industries', ['url' => 'industries', 'icon' => 'flaticon-layers']);
            $menu->get('industries')->add('New Industry', ['url' => 'industries/create', 'icon' => 'flaticon-layers']);

            $menu->add('Services' , ['icon'=>'flaticon-layers']);
            $menu->get('services')->add('Services', ['url' => 'services', 'icon' => 'flaticon-layers']);
            $menu->get('services')->add('New Service', ['url' => 'services/create', 'icon' => 'flaticon-layers']);

        });
        return $next($request);
    }
}
