<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        switch ($this->step) {
            case 'step-1':
                $rules = [
                    'company_name' => 'required',
                    'min_project_size' => 'required',
                    'hourly_rate' => 'required',
                    'company_size' => 'required',
                    'year_founded' => 'required',
                    'tagline' => 'required',
                    'summary' => 'required',
                ];
                break;

            case 'step-2':
                $rules = [
                    'locations.*.country' => 'required',
                    'locations.*.state' => 'required',
                    'locations.*.city' => 'required',
                    'locations.*.address' => 'required',
                    'locations.*.phone' => 'required',
                    'locations.*.postal_code' => 'required',
                    'locations.*.is_head' => 'required',
                ];
                break;
            case 'step-3':
                $rules = [
                    'website' => 'required',
                    'email' => 'required|email',
                ];
                break;
            case 'step-4':
                $rules = [
                    'services.*.percentage' => 'required|numeric',
                    'industries.*.percentage' => 'required|numeric',
                ];
                break;

            case 'step-6':
                $rules = [
                    'admin_email' => 'required|email',
                    'admin_phone' => 'required',
                ];
                break;
            default:
                break;
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        switch ($this->step) {
            case 'step-2':
                $messages = [
                    'locations.*.country.required' => 'The country field is required.',
                    'locations.*.state.required' => 'The state field is required.',
                    'locations.*.city.required' => 'The city field is required.',
                    'locations.*.address.required' => 'The address field is required.',
                    'locations.*.phone.required' => 'The phone field is required.',
                    'locations.*.postal_code.required' => 'The postal code field is required.',
                    'locations.*.is_head.required' => 'The is headquater field is required.',
                ];
                break;

            case 'step-4':
                $messages = [
                    'services.*.percentage.required' => 'The percentage field is required.',
                    'industries.*.percentage.required' => 'The percentage field is required.',
                ];
                break;
            
            default:
                break;
        }

        return $messages;
    }
}
