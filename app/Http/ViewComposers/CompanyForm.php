<?php

namespace App\Http\ViewComposers;

use App\Models\Common\Country;
use Illuminate\View\View;

class CompanyForm
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $projectSizeValues = ['$1000+', '$5000+'];
        $companySizeValues = ['Freelancer', '2-9', '10-49', '50-249'];
        $hourlyRateValues = ['<$25', '$25-$49', '$50-$99', '$100-$149'];
        $countryValues = Country::orderBy('name', 'asc')->pluck('name', 'id');

        $view->with(compact(
            'projectSizeValues', 
            'companySizeValues',
            'hourlyRateValues',
            'countryValues'
        ));
    }
}
