<?php

namespace App\Models\Auth;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $gender
 * @property string $phone
 * @property string $date_of_birth
 * @property string $facebook
 * @property string $twitter
 * @property string $website
 * @property string $linkedin
 * @property string $instagram
 * @property int $country_id
 * @property string $bio
 * @property string $last_login_at
 * @property string $last_login_ip
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 */
class User extends Authenticatable
{
    use Notifiable;
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'email_verified_at', 'password', 'gender', 'phone', 'date_of_birth', 'facebook', 'twitter', 'website', 'linkedin', 'instagram', 'country_id', 'bio', 'last_login_at', 'last_login_ip', 'remember_token', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }

    public function faqs()
    {
        return $this->hasMany('App\Models\Company\Faq', 'user_id');
    }


    public function companyLocations()
    {
        return $this->hasMany('App\Models\Company\CompanyLocation', 'user_id');
    }


    public function founders()
    {
        return $this->hasMany('App\Models\Company\Founder', 'user_id');
    }


    public function portfolios()
    {
        return $this->hasMany('App\Models\Company\Portfolio', 'user_id');
    }


    public function industries()
    {
        return $this->belongsTo('App\Models\Company\Industry', 'user_id');
    }

    public function visitor()
    {
        return $this->hasOne('App\Models\Visitor\Visitor', 'user_id');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Common\Language', 'company_language', 'user_id', 'language_id')->withPivot('priority')->withTimestamps();
    }




    //Media Collections for User
    public function registerMediaCollections()
    {
        $this->addMediaCollection('user_pic')
            ->singleFile()
            ->useDisk('public')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb')
                    ->keepOriginalImageFormat()
                    ->fit('crop', 100, 100);

                $this
                    ->addMediaConversion('small')
                    ->keepOriginalImageFormat()
                    ->fit('crop', 300, 300);

                $this
                    ->addMediaConversion('medium')
                    ->keepOriginalImageFormat()
                    ->fit('crop', 500, 500);

                $this
                    ->addMediaConversion('large')
                    ->keepOriginalImageFormat()
                    ->fit('crop', 800, 800);
            });

        $this->addMediaCollection('social_pic')
            ->useDisk('public')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb')
                    ->keepOriginalImageFormat()
                    ->fit('crop', 100, 100);
            });
    }


    /**
     * Get the user's picture.
     *
     * @param  string  $value
     * @return void
     */
    public function getAvatarAttribute($avatar)
    {

        $userAvatar = $this->getMedia('user_pic');
        if(isset($userAvatar[0]))
            return $userAvatar[0]->getFullUrl('thumb');

        $socialAvatar = $this->getMedia('social_pic');
        if(isset($socialAvatar[0]))
            return $socialAvatar[0]->getFullUrl('thumb');

        // If no pic available
        try {
                // Check for gravatar
                $gravatar = 'https://www.gravatar.com/avatar/' . md5(strtolower($this->getAttribute('email'))).'?size=100&d=404';

                $client = new \GuzzleHttp\Client(['verify' => false]);

                $client->request('GET', $gravatar)->getBody()->getContents();

                return $gravatar;
            } catch (\Exception $e) {
                // 404 Not Found
                return null;
            }
    }

}
