<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $state_id
 * @property string $created_at
 * @property string $updated_at
 */
class City extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'name', 'state_id', 'created_at', 'updated_at'];

    public function state()
    {
        return $this->belongsTo('App\Models\Common\State', 'state_id');
    }

}
