<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $iso2_code
 * @property string $iso3_code
 * @property string $phone_code
 * @property string $created_at
 * @property string $updated_at
 */
class Country extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'name', 'iso2_code', 'iso3_code', 'phone_code', 'created_at', 'updated_at'];


    public function states()
    {
        return $this->hasMany('App\Models\Common\State', 'country_id');
    }

    public function currencies()
    {
        return $this->belongsToMany('App\Models\Common\Currency', 'country_currency', 'country_id', 'currency_id')->withPivot('priority')->withTimestamps();
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Common\Language', 'country_language', 'country_id', 'language_id')->withPivot('priority')->withTimestamps();
    }

    public function visitorLocations()
    {
        return $this->hasMany('App\Models\Visitor\VisitorLocation', 'country_id');
    }

    public function companyLocations()
    {
        return $this->hasMany('App\Models\Company\CompanyLocation', 'country_id');
    }

}
