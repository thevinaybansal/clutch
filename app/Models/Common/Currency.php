<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $symbol
 * @property string $created_at
 * @property string $updated_at
 */
class Currency extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currencies';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'symbol', 'created_at', 'updated_at'];

    public function country()
    {
        return $this->belongsToMany('App\Models\Common\Country', 'country_currency', 'currency_id', 'country_id')->withPivot('priority')->withTimestamps();
    }

}
