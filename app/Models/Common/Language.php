<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $native_name
 * @property string $iso1
 * @property string $iso2t
 * @property string $iso2b
 * @property string $iso3
 * @property string $created_at
 * @property string $updated_at
 */
class Language extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'name', 'native_name', 'iso1', 'iso2t', 'iso2b', 'iso3', 'created_at', 'updated_at'];


    public function companies()
    {
        return $this->belongsToMany('App\Models\Company\Company', 'company_language', 'language_id', 'company_id')->withPivot('priority')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\Auth\User', 'user_language', 'language_id', 'user_id')->withPivot('priority')->withTimestamps();
    }

    public function country()
    {
        return $this->belongsToMany('App\Models\Common\Country', 'country_currency', 'currency_id', 'country_id')->withPivot('priority')->withTimestamps();
    }
}
