<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $country_id
 * @property string $created_at
 * @property string $updated_at
 */
class State extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'name', 'country_id', 'created_at', 'updated_at'];


    public function cities()
    {
        return $this->hasMany('App\Models\Common\City', 'state_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Common\Country', 'state_id');
    }

}
