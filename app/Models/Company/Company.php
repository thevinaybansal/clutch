<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * @property integer $id
 * @property string $name
 * @property string $tagline
 * @property string $slug
 * @property string $min_project_size
 * @property string $company_size
 * @property string $hourly_rate
 * @property string $year_founded
 * @property string $summary
 * @property string $clients_list
 * @property string $ga_tracking_id
 * @property string $cover_image
 * @property string $employee_count
 * @property string $vat_id
 * @property string $tax_id
 * @property string $legal_name
 * @property string $logo_image
 * @property string $website
 * @property string $facebook
 * @property string $linkedin
 * @property string $instagram
 * @property string $twitter
 * @property string $email
 * @property string $phone
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class Company extends Model
{

    use SoftDeletes;
    // use HasMediaTrait;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'tagline', 'slug', 'min_project_size', 'company_size', 'hourly_rate', 'year_founded', 'summary', 'clients_list', 'ga_tracking_id', 'cover_image', 'employee_count', 'vat_id', 'tax_id', 'legal_name', 'website', 'facebook', 'linkedin', 'instagram', 'twitter', 'email', 'phone', 'deleted_at', 'created_at', 'updated_at'];




    public function users()
    {
        return $this->hasMany('App\Models\Auth\User', 'company_id');
    }

    public function locations()
    {
        return $this->hasMany('App\Models\Company\CompanyLocation', 'company_id');
    }


    public function faqs()
    {
        return $this->hasMany('App\Models\Company\Faq', 'company_id');
    }


    public function founders()
    {
        return $this->hasMany('App\Models\Company\Founder', 'company_id');
    }


    public function portfolios()
    {
        return $this->hasMany('App\Models\Company\Portfolio', 'company_id');
    }


    public function services()
    {
        return $this->belongsToMany('App\Models\Company\Service', 'company_service', 'company_id', 'service_id')->withPivot('focus_percentage')->withTimestamps();
    }

    public function industries()
    {
        return $this->belongsToMany('App\Models\Company\Industry', 'company_industry', 'company_id', 'industry_id')->withPivot('focus_percentage')->withTimestamps();
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Common\Language', 'company_language', 'company_id', 'language_id')->withPivot('priority')->withTimestamps();
    }

    public function visitorActivities()
    {
        return $this->hasMany('App\Models\Visitor\VisitorActivity', 'company_id');
    }



    //Media Collections for Company
    public function registerMediaCollections()
    {
        $this->addMediaCollection('company_logo')
        ->singleFile()
        ->registerMediaConversions(function (Media $media) {
            $this
                ->addMediaConversion('thumb')
                ->format('jpg')
                ->fit('crop', 100, 100);

            $this
                ->addMediaConversion('small')
                ->fit('crop', 300, 300);

            $this
                ->addMediaConversion('medium')
                ->fit('crop', 500, 500);

            $this
                ->addMediaConversion('large')
                ->format('jpg')
                ->fit('crop', 800, 800);
        });
    }

}
