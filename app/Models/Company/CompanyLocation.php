<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $slug
 * @property string $company_id
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country_id
 * @property string $postal_code
 * @property string $phone_number
 * @property string $employee_count
 * @property boolean $is_headquarter
 * @property string $created_at
 * @property string $updated_at
 */
class CompanyLocation extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_locations';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'company_id', 'user_id', 'address', 'city', 'state', 'country_id', 'postal_code', 'phone_number', 'employee_count', 'is_headquarter', 'created_at', 'updated_at'];



    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Common\Country', 'state_id');
    }
}
