<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $question
 * @property string $answer
 * @property string $faq_type
 * @property string $company_id
 * @property string $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Faq extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'faqs';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['question', 'answer', 'faq_type', 'company_id', 'user_id', 'created_at', 'updated_at'];


    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }


    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id');
    }

}
