<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property string $email_id
 * @property string $phone
 * @property string $photo
 * @property string $company_id
 * @property string $created_at
 * @property string $updated_at
 */
class Founder extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'founders';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'email_id', 'phone', 'photo', 'company_id', 'user_id', 'created_at', 'updated_at'];


    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }


    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id');
    }

}
