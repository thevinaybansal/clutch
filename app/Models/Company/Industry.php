<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Industry extends Model
{

    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'industries';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'name', 'created_at', 'updated_at'];


    public function companies()
    {
        return $this->belongsToMany('App\Models\Company\Company', 'company_industry', 'industry_id', 'company_id')->withPivot('focus_percentage')->withTimestamps();
    }


    public function users()
    {
        return $this->hasMany('App\Models\Auth', 'user_id');
    }

}
