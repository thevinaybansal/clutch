<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $slug
 * @property string $link
 * @property string $title
 * @property string $summary
 * @property string $project_cost
 * @property string $category_id
 * @property string $industry_id
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 */
class Portfolio extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'portfolios';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'link', 'title', 'summary', 'project_cost', 'category_id', 'industry_id', 'image', 'created_at', 'updated_at', 'user_id'];

    public function getImageAttribute($value)
    {
        return asset($value);
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }


    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id');
    }

}
