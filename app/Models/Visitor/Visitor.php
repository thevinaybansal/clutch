<?php

namespace App\Models\Visitor;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $ip_address
 * @property string $browser
 * @property string $os
 * @property string $created_at
 * @property string $updated_at
 */
class Visitor extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'visitors';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'ip_address', 'browser', 'os', 'is_logged_in', 'user_id', 'created_at', 'updated_at'];


    public function activities()
    {
        return $this->hasMany('App\Models\Visitor\VisitorActivity', 'visitor_id');
    }


    public function locations()
    {
        return $this->hasOne('App\Models\Visitor\VisitorLocation', 'visitor_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id');
    }

}
