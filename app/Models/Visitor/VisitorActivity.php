<?php

namespace App\Models\Visitor;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $type
 * @property string $data
 * @property string $company_id
 * @property string $page_url
 * @property string $page_type
 * @property string $created_at
 * @property string $updated_at
 */
class VisitorActivity extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'visitor_activities';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['type', 'data', 'company_id', 'page_url', 'page_type', 'visitor_id', 'created_at', 'updated_at'];


    public function visitor()
    {
        return $this->belongsTo('App\Models\Visitor\Visitor', 'visitor_id');
    }


    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

}
