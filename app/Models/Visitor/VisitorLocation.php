<?php

namespace App\Models\Visitor;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $country_id
 * @property string $city
 * @property string $postal_code
 * @property string $longitude
 * @property string $latitude
 * @property string $isp
 * @property string $org_name
 * @property string $created_at
 * @property string $updated_at
 */
class VisitorLocation extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'visitors_locations';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['visitor_id', 'country_id', 'city', 'postal_code', 'longitude', 'latitude', 'isp', 'org_name', 'created_at', 'updated_at'];


    public function visitor()
    {
        return $this->belongsTo('App\Models\Visitor\Visitor', 'visitor_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Common\Country', 'country_id');
    }

}
