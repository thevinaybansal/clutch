<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Form components
        Form::component('textGroup', 'partials.form.text_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('emailGroup', 'partials.form.email_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('passwordGroup', 'partials.form.password_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('numberGroup', 'partials.form.number_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('selectGroup', 'partials.form.select_group', [
            'name', 'text', 'icon', 'values', 'selected' => null, 'attributes' => ['required' => 'required'], 'col' => 'col-md-6',
        ]);

        Form::component('multiSelectGroup', 'partials.form.multiselect_group', [
            'name', 'text', 'icon', 'values', 'selected' => null, 'attributes' => ['required' => 'required'], 'col' => 'col-md-6',
        ]);

        Form::component('selectSearchGroup', 'partials.form.select_search_group', [
            'name', 'text', 'icon', 'values', 'selected' => null, 'attributes' => ['required' => 'required'], 'col' => 'col-md-6',
        ]);

        Form::component('textareaGroup', 'partials.form.textarea_group', [
            'name', 'text', 'value' => null, 'attributes' => ['rows' => '3'], 'col' => 'col-md-12',
        ]);

        Form::component('dateGroup', 'partials.form.date_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('dateTimeGroup', 'partials.form.date_time_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('dateRangeGroup', 'partials.form.date_range_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('customDateRangeGroup', 'partials.form.custom_date_range_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('dateTimeRangeGroup', 'partials.form.date_time_range_group', [
            'name', 'text', 'icon', 'attributes' => ['required' => 'required'], 'value' => null, 'col' => 'col-md-6',
        ]);

        Form::component('saveButtons', 'partials.form.save_buttons', [
            'cancel', 'col' => 'col-md-12',
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}