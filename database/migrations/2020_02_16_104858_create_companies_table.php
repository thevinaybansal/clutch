<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('tagline')->nullable();
            $table->string('slug');
            $table->string('min_project_size')->nullable();
            $table->string('company_size')->nullable();
            $table->string('hourly_rate')->nullable();
            $table->string('year_founded')->nullable();
            $table->text('summary')->nullable();
            $table->text('clients_list')->nullable();
            $table->string('ga_tracking_id')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('employee_count')->nullable();
            $table->string('vat_id')->nullable();
            $table->string('tax_id')->nullable();
            $table->string('legal_name')->nullable();
            $table->string('logo_image')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
