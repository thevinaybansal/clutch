/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '../admin-theme/tools/webpack/vendors/global';
import '../admin-theme/tools/webpack/scripts';

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     // console.log(key.split('/').splice(2, 2).join('').split('.')[0]);
    
//     Vue.component(key.split('/').splice(2, 2).join('').split('.')[0], files(key).default)
// })

Vue.component('company-create', require('./components/CompanyCreate.vue').default);
Vue.component('company-create-basic-info', require('./components/CompanyForm/BasicInfo.vue').default);
Vue.component('company-create-locations', require('./components/CompanyForm/Locations.vue').default);
Vue.component('company-create-lead', require('./components/CompanyForm/Lead.vue').default);
Vue.component('company-create-focus', require('./components/CompanyForm/Focus.vue').default);
Vue.component('company-create-portfolio', require('./components/CompanyForm/Portfolio.vue').default);
Vue.component('company-create-admin-info', require('./components/CompanyForm/AdminInfo.vue').default);

// Use plugins

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
