@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')
<!--Begin::Dashboard-->

<div class="row">
                        
<div class="kt-portlet">

{!! Form::open() !!}


{{ Form::textGroup('name', 'abc', 'fa fa-globe') }}     
{{ Form::emailGroup('email', 'abc', 'fa fa-globe') }}     
{{ Form::passwordGroup('password', 'abc', 'fa fa-globe') }}     
{{ Form::numberGroup('number', 'abc', 'fa fa-globe') }}     
{{ Form::selectGroup('select', 'abc', 'fa fa-globe', ["aa", "bb"]) }}     
{{ Form::multiSelectGroup('multiselect', 'abc', 'fa fa-globe', ["aa", "bb"]) }}     
{{ Form::selectSearchGroup('selectsearch', 'abc', 'fa fa-globe', ["aa", "bb"]) }}     
{{ Form::textareaGroup('textarea', 'abc') }}     
{{ Form::dateGroup('date', 'abc', 'fa fa-globe') }}     
{{ Form::dateTimeGroup('datetime', 'abc', 'fa fa-globe') }}     
{{ Form::dateRangeGroup('daterange', 'abc', 'fa fa-globe') }}     
{{ Form::customDateRangeGroup('customedaterange', 'abc', 'fa fa-globe') }}     
{{ Form::dateTimeRangeGroup('datetimerange', 'abc', 'fa fa-globe') }}     
{{ Form::saveButtons('save', 'abc', 'fa fa-globe') }}     




{!! Form::close() !!}

</div>

</div>

<!--End::Dashboard 1-->



@endsection









@section('js')
@endsection



@section('scripts')
@endsection



@section('css')
@endsection