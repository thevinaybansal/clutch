@extends('layouts.admin')

@section('title', 'New Company')

@section('content')
<company-create></company-create>
@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection