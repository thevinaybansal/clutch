@extends('layouts.admin')

@section('title', 'Edit Company')

@section('content')
<company-create id="{{ $company->id }}"></company-create>
@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection