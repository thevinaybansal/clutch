<div class="kt-wizard-v2__form row">

    {{ Form::textGroup('company_name', 'Compayny name', 'fa fa-globe', ['placeholder' => 'Enter company name'], null, 'col-12') }}
    {{ Form::selectGroup('min_project_size', 'What is your minimum project size?', 'fa fa-globe', $projectSizeValues, null, ['placeholder' => 'Select a value'], 'col-6') }}
    {{ Form::selectGroup('hourly_rate', 'What is your hourly rate?', 'fa fa-globe', $hourlyRateValues, null, ['placeholder' => 'Select a value'], 'col-6') }}
    {{ Form::selectGroup('company_size', 'What size is your company?', 'fa fa-globe', $companySizeValues, null, ['placeholder' => 'Select a value'], 'col-6') }}
    {{ Form::numberGroup('year_founded', 'When was your company founded?', 'fa fa-globe', ['placeholder' => '2010'], null, 'col-6') }}
    {{ Form::textGroup('teg_line', 'Tagline', 'fa fa-globe', ['placeholder' => 'Enter tagline'], null, 'col-12') }}
    {{ Form::textareaGroup('summary', 'Tagline', null, ['placeholder' => 'Short description about your company'], 'col-12') }}
    
</div>