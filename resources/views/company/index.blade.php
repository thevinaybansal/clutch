@extends('layouts.admin')

@section('title', 'Companies')

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Companies
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body" style="padding: 0px;">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Company name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($companies as $key =>  $company)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $company->name }}</td>
                    <td>
                        <a href="{{ route('company.edit', $company) }}" class="btn btn-primary" href="">Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection
