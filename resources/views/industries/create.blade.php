@extends('layouts.admin')

@section('title', 'New Industry')

@section('content')
	
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Industry Form
				</h3>
			</div>
		</div>
		<!--begin::Form-->
		@include('includes.flashMsg')
	    {!! Form::open(['url' => url('industries'), 'class' => 'kt-form', 'data-parsley-validate novalidate', 'files' => true]) !!}

	        @include('industries.form')
	      
	    {!! Form::close() !!}
		<!--end::Form-->
	</div>

@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection