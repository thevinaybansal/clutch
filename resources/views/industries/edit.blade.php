@extends('layouts.admin')

@section('title', 'Edit Industry')

@section('content')
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Industry Form
				</h3>
			</div>
		</div>
	    @include('includes.flashMsg')
	    {!! Form::model($industry, ['method' => 'PUT', 'route' => ['industries.update', $industry->id],'class' => 'form form-group', 'data-parsley-validate novalidate', 'files' => true]) !!}
	            
	        @include('industries.form')
	      
	    {!! Form::close() !!}
	</div>
@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection