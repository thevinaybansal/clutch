@extends('layouts.admin')

@section('title', 'Industries')

@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<!-- <i class="kt-font-brand flaticon2-line-chart"></i> -->
			</span>
			<h3 class="kt-portlet__head-title">
				Industries
			</h3>
		</div>
	</div>

	<div class="kt-portlet__body">
		@include('includes.flashMsg')
    	<table id="industry-table" class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer">
			<thead>
				<tr>
					<th>Id</th>
					<th>Slug</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if(count($industries))
					@foreach($industries as $industry)
						<tr>
							<td>{{ $industry->id }}</td>
							<td>{{ $industry->slug }}</td>
							<td>{{ $industry->name }}</td>
							<td>
								<div class="btn-group">
				                    <a href="{{ url("industries/$industry->id/edit") }}" class="btn btn-primary" title="edit"><i class="fa fa-edit"></i></a> 
				                    <a href="{{ url("industries/$industry->id/delete") }}" class="btn btn-danger btn-delete-record" title="delete" data-id="{{ $industry->id }}"><i class="fa fa-trash"></i></a> 
					            </div>
					        </td>
						</tr>
					@endforeach
				@else
			    	<td colspan="4" align="center">No industries found.</td>
			    @endif
			</tbody>
		</table>

		<div class="col-md-12">
			<div class="col-md-7 pull-right">
				{{ $industries->render() }}
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection