<!DOCTYPE html>

<html lang="en">

	@include('partials.admin.head')

	{{-- begin::Body --}}
	<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    {{-- Page Loader --}}
    @include('partials.admin.page-loader')

    
    {{-- begin:: Page --}}


    {{-- Header Mobile --}}
    @include('partials.admin.header.header-mobile')


		<div id="app" class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">


        {{-- Aside Menu --}}
				@include('partials.admin.asidemenu.base')

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

          {{-- Header --}}
					@include('partials.admin.header.base')

          <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

            {{-- SubHeader --}}
            @include('partials.admin.header.subheader')

            {{-- Content --}}
						@include('partials.admin.content')
					</div>
					
					@include('partials.admin.footer')
					
				</div>
			</div>
		</div>
		<!-- end:: Page -->

    @include('partials.admin.quickpanel')
    @include('partials.admin.scrolltop')
    @include('partials.admin.toolbar')
    @include('partials.admin.chat')
		
    {{-- Footer Scripts --}}
		@include('partials.admin.footscripts')

	</body>
	<!-- end::Body -->
</html>