{{-- begin:: Aside menu --}}

{{-- To display the close button of the panel --}}
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

  {{-- Logo on Header --}}
  @include('partials.admin.asidemenu.brand')
  {{-- Menu --}}
  @include('partials.admin.asidemenu.menu')
</div>
{{-- end:: Aside --}}


