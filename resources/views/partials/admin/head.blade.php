{{-- begin::Head --}}
	<head>

		<meta charset="utf-8" />
		<!-- CSRF Token -->
    {{ csrf_field() }}

    <title>@yield('title')</title>
		<meta name="description" content="@yield('description')">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		@stack('css')
    @stack('stylesheet')
		<!--end::Page Vendors Styles -->

    {{-- Styles --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >

    {{-- Favicon --}}
		<link rel="shortcut icon" href="{{ asset(config('directory.favicon') . 'favicon.ico') }}" />
	</head>
{{-- end::Head --}}