{{-- begin:: Header --}}
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

  {{-- Header Menu --}}
  @include('partials.admin.header.menu')

  {{-- Header Topbar --}}
  @include('partials.admin.header.topbar.base')
</div>
{{-- end:: Header --}}