{{-- begin: Header Menu --}}
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
  <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
    <ul class="kt-menu__nav ">
      {{-- TODO : Implement Dynamic Header menu --}}
      <li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="" class="kt-menu__link "><span class="kt-menu__link-text">Visit Site</span></a></li>
    </ul>
  </div>
</div>
{{-- end: Header Menu --}}