{{-- begin:: Header Topbar --}}
<div class="kt-header__topbar">


  @include('partials.admin.header.topbar.search')
  @include('partials.admin.header.topbar.notifications')
  @include('partials.admin.header.topbar.quick-actions')
  @include('partials.admin.header.topbar.my-cart')
  @include('partials.admin.header.topbar.quick-panel')
  @include('partials.admin.header.topbar.languages')
  @include('partials.admin.header.topbar.user')

</div>

{{-- end:: Header Topbar --}}