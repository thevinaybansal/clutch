{{-- begin: User Bar  --}}
<div class="kt-header__topbar-item kt-header__topbar-item--user">
  <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
    <div class="kt-header__topbar-user">
      <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi </span>
        <span class="kt-header__topbar-username kt-hidden-mobile">{{ $user->firstName ?? 'There!' }}</span>
          
          @if($user->avatar ?? '')
            <img alt="Pic" src="{{ $user->avatar ?? '' }}" />
          @else
            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ $user->firstLetter ?? 'A' }}</span>
          @endif
        </div>
    </div>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

      {{-- User DropDown --}}
      @include('partials.admin.header.topbar.user-dropdown')

    </div>
</div>
{{-- end: User Bar  --}}