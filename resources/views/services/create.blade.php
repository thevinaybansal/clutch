@extends('layouts.admin')

@section('title', 'New Service')

@section('content')

	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Service Form
				</h3>
			</div>
		</div>

	    @include('includes.flashMsg')
	    {!! Form::open(['url' => url('services'), 'class' => 'form form-group', 'data-parsley-validate novalidate', 'files' => true]) !!}

	        @include('services.form')
	      
	    {!! Form::close() !!}
	</div>

@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection