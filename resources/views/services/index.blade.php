@extends('layouts.admin')

@section('title', 'Services')

@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<!-- <i class="kt-font-brand flaticon2-line-chart"></i> -->
			</span>
			<h3 class="kt-portlet__head-title">
				Services
			</h3>
		</div>
	</div>

	<div class="kt-portlet__body">
    	<table id="industry-table" class="table table-bordered table-hover display">
			<thead>
				<tr>
					<th>Id</th>
					<th>Slug</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if(count($services))
					@foreach($services as $service)
						<tr>
							<td>{{ $service->id }}</td>
							<td>{{ $service->slug }}</td>
							<td>{{ $service->name }}</td>
							<td>
								<div class="btn-group">
				                    <a href="{{ url("services/$service->id/edit") }}" class="btn btn-primary" title="edit"><i class="fa fa-edit"></i></a> 
				                    <a href="{{ url("services/$service->id/delete") }}" class="btn btn-danger btn-delete-record" title="delete"><i class="fa fa-trash"></i></a> 
					            </div>
					        </td>
						</tr>
					@endforeach
				@else
			    	<td colspan="4" align="center">No services found.</td>
			    @endif
			</tbody>
		</table>

		<div class="col-md-12">
			<div class="col-md-7 pull-right">
				{{ $services->render() }}
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
@endsection

@section('scripts')
@endsection

@section('css')
@endsection