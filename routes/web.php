<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Dashboard.admin');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('companies', ['uses' => 'CompanyController@index', 'as' => 'companies']);
Route::get('company/create', ['uses' => 'CompanyController@create', 'as' => 'company.create']);
Route::post('company/create', ['uses' => 'CompanyController@save', 'as' => 'company.create']);
Route::get('company/{id}', ['uses' => 'CompanyController@find', 'as' => 'company.find']);
Route::get('company/{company}/edit', ['uses' => 'CompanyController@edit', 'as' => 'company.edit']);
Route::get('company/{company}/portfolios', ['uses' => 'CompanyController@getPortfolios', 'as' => 'company.portfolios']);
Route::get('company/{company}/locations', ['uses' => 'CompanyController@getLocations', 'as' => 'company.locations']);
Route::get('company/{company}/industries', ['uses' => 'CompanyController@getIndustries', 'as' => 'company.industries']);
Route::get('company/{company}/services', ['uses' => 'CompanyController@getServices', 'as' => 'company.services']);

Route::get('industries/{id}/delete','IndustryController@destroy');
Route::resource('industries', 'IndustryController');

Route::get('services/{id}/delete','ServiceController@destroy');
Route::resource('services', 'ServiceController');

Route::get('countries', ['uses' => 'CountryController@index', 'as' => 'countries']);
Route::get('states', ['uses' => 'StateController@index', 'as' => 'states']);
Route::get('cities', ['uses' => 'CityController@index', 'as' => 'cities']);

Route::get('portfolios/{portfolio}/delete', ['uses' => 'PortfolioController@delete', 'as' => 'porftolios.delete']);
